package org.jeecg.image.phantomJs;

import org.jeecg.common.phantomjs.FileUtils;
import org.jeecg.common.phantomjs.Html2ImageByJsWrapper;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @package: com.cunguan.seceum
 * @description: 只能是静态页面
 * @author: 孟凡霄
 * @date: Created in 2020/11/17 10:59 上午
 * @copyright: Copyright (c) 2020
 * @version: V1.0
 * @modified: 孟凡霄
 */


public class Html2ImageByJsWrapperTest {

    @Test
    public void testRender() throws IOException, InterruptedException {
        String url = "http://127.0.0.1:8000/test.html?id=11232432342345";
        File file = Html2ImageByJsWrapper.renderHtml2ImageFile(url);
        FileUtils.writeToLocal(
                "/Users/mengfanxiao/Documents/work/cunzheng/gongzheng/MicroEvidence/jeecg-boot/jeecg-boot-module-gz/src/test/java/org/jeecg/a.png",
                new FileInputStream(file));
    }
}
