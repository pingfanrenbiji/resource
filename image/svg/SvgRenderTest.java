package org.jeecg.image.svg;

import com.github.hui.quick.plugin.svg.SvgRenderWrapper;
import org.apache.commons.io.IOUtils;
import org.jeecg.common.phantomjs.FileUtils;
import org.junit.Test;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yihui on 2018/1/14.
 */
public class SvgRenderTest {

    @Test
    public void testGenPng() {
        try {


            Map<String, Object> map = new HashMap<>();
            map.put("code", "1111111110");

            BufferedImage img = SvgRenderWrapper.convertToJpegAsImg(
                    "/Users/mengfanxiao/Documents/work/cunzheng/gongzheng/MicroEvidence/jeecg-boot/jeecg-boot-module-gz/src/test/java/org/jeecg/image/svg/zhengshu.svg",
                    map);

            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            ImageOutputStream imOut = ImageIO.createImageOutputStream(bs);
            ImageIO.write(img, "jpg", imOut);
            InputStream inputStream = new ByteArrayInputStream(bs.toByteArray());

            OutputStream outStream = new FileOutputStream("/Users/mengfanxiao/Documents/work/cunzheng/gongzheng/MicroEvidence/jeecg-boot/jeecg-boot-module-gz/src/test/java/org/jeecg/image/svg/captcha1.jpg");
            IOUtils.copy(inputStream, outStream);
            inputStream.close();
            outStream.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testFixPngImg() {
            try {
                BufferedImage img = SvgRenderWrapper.convertToJpegAsImg("/Users/mengfanxiao/Documents/work/cunzheng/gongzheng/MicroEvidence/jeecg-boot/jeecg-boot-module-gz/src/test/java/org/jeecg/image/svg/card.svg", new HashMap<>());
                System.out.println("---");
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
