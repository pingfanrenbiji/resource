package com.cunguan.seceum.util.file;

import alluxio.AlluxioURI;
import alluxio.client.file.FileInStream;
import alluxio.client.file.FileOutStream;
import alluxio.client.file.FileSystem;
import alluxio.conf.AlluxioConfiguration;
import alluxio.conf.AlluxioProperties;
import alluxio.conf.InstancedConfiguration;
import alluxio.conf.PropertyKey;
import alluxio.exception.AlluxioException;
import alluxio.grpc.CreateFilePOptions;
import alluxio.grpc.WritePType;
import alluxio.util.ConfigurationUtils;
import com.cunguan.seceum.util.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @package: com.cunguan.seceum
 * @description:
 * @author: 孟凡霄
 * @date: Created in 2020/10/20 2:09 下午
 * @copyright: Copyright (c) 2020
 * @version: V1.0
 * @modified: 孟凡霄
 */

@Component
public class AlluxioUtilsDemo {

    private static Logger logger= LoggerFactory.getLogger(AlluxioUtilsDemo.class);

    /**
     *
     *
     * @description: 从alluxio上下载文件
     * @return:
     * @author: 孟凡霄
     * @time: 2021/1/22 10:55 上午
     */

    public static void uploadAlluxio() throws IOException, AlluxioException {
        /**配置alluxio的服务地址**/
        AlluxioProperties alluxioProperties=ConfigurationUtils.defaults();
        alluxioProperties.set(PropertyKey.SECURITY_LOGIN_USERNAME, "alluxio");
        alluxioProperties.set(PropertyKey.MASTER_RPC_ADDRESSES, "127.0.0.1:19998");
        AlluxioConfiguration alluxioConf = new InstancedConfiguration(alluxioProperties);
        /**定义上传到alluxio的文件路径**/
        AlluxioURI outputPath = new AlluxioURI("/test/99f1e41e-778a-45c3-fa98-4e4dbeb60f0a.csv");
        FileSystem fileSystem = FileSystem.Factory.create(alluxioConf);
        /**读取文件流**/
        File file=new File("/Users/mengfanxiao/Documents/stream/code/seceum-data-processing/src/main/resources/99f1e41e-778a-45c3-fa98-4e4dbeb60f0a.csv");
        FileInputStream is=new FileInputStream(file);
        CreateFilePOptions options = CreateFilePOptions.newBuilder().setWriteType(WritePType.CACHE_THROUGH).setRecursive(true).build();
        /**上传文件 并将文件流写入alluxio文件中**/
        FileOutStream os = fileSystem.createFile(outputPath, options);
        IOUtils.copy(is, os);
        is.close();
        os.close();
    }


    /**
     *
     *
     * @description: 从alluxio上下载文件
     * @return:
     * @author: 孟凡霄
     * @time: 2021/1/22 10:55 上午
     */

    public static void downLoadAlluxio() throws IOException, AlluxioException {
        /**配置alluxio服务器**/
        AlluxioProperties alluxioProperties=ConfigurationUtils.defaults();
        alluxioProperties.set(PropertyKey.SECURITY_LOGIN_USERNAME, "alluxio");
        alluxioProperties.set(PropertyKey.MASTER_RPC_ADDRESSES, "81.69.234.232:19998");
        AlluxioConfiguration alluxioConf = new InstancedConfiguration(alluxioProperties);
        /**指定alluxio文件路径**/
        AlluxioURI inputPath = new AlluxioURI("/test/38f872fd0cf0492990ff7541dc19a6df.csv");
        FileSystem fileSystem = FileSystem.Factory.create(alluxioConf);
        /**读取alluxio文件**/
        FileInStream is = fileSystem.openFile(inputPath);
        /**将读取到的文件流写入本地文件**/
        String filePath= "/Users/mengfanxiao/Documents/stream/code/seceum-data-processing/src/main/resources/38f872fd0cf0492990ff7541dc19a6df.csv";
        FileUtils.writeToLocal(filePath, is);
    }

}
