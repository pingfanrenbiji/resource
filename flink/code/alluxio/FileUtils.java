package com.cunguan.seceum.util;

import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * 项目名称：seceum-studio
 * 类 名 称：FileUtils
 * 类 描 述：文件操作类
 * 创建时间：2020/8/31 3:22 下午
 * @author yangjinglei
 */
public class FileUtils {

    private FileUtils() { }

    /**
     * 将InputStream写入本地文件
     * @param destination 写入本地目录
     * @param input	输入流
     */
    public static void writeToLocal(String destination, InputStream input) {
        int index;
        byte[] bytes = new byte[1024];
        try (FileOutputStream downloadFile = new FileOutputStream(destination)) {
            while ((index = input.read(bytes)) != -1) {
                downloadFile.write(bytes, 0, index);
                downloadFile.flush();
            }
            input.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
