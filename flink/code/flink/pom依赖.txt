

		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-connector-hive_${scala.binary.version}</artifactId>
			<version>${flink.version}</version>
		</dependency>

		<!-- Hadoop Dependencies -->
		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-hadoop-compatibility_2.11</artifactId>
			<version>1.9.0</version>
		</dependency>

		<!-- Hive 2.3.4 is built with Hadoop 2.7.2.
			We pick 2.7.5 which flink-shaded-hadoop is pre-built with,
			 but users can pick their own hadoop version,
			 as long as it's compatible with Hadoop 2.7.2 -->
		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-shaded-hadoop-2-uber</artifactId>
			<version>2.7.5-8.0</version>
			<exclusions>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Hive Metastore -->
		<dependency>
			<groupId>org.apache.hive</groupId>
			<artifactId>hive-exec</artifactId>
			<version>${hive.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.apache.httpcomponents</groupId>
					<artifactId>httpclient</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.httpcomponents</groupId>
					<artifactId>httpcore</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.hive</groupId>
					<artifactId>hive-vector-code-gen</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.hive</groupId>
					<artifactId>hive-llap-tez</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.hive</groupId>
					<artifactId>hive-shims</artifactId>
				</exclusion>
				<exclusion>
					<groupId>commons-codec</groupId>
					<artifactId>commons-codec</artifactId>
				</exclusion>
				<exclusion>
					<groupId>commons-httpclient</groupId>
					<artifactId>commons-httpclient</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.logging.log4j</groupId>
					<artifactId>log4j-slf4j-impl</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.antlr</groupId>
					<artifactId>antlr-runtime</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.antlr</groupId>
					<artifactId>ST4</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.ant</groupId>
					<artifactId>ant</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.commons</groupId>
					<artifactId>commons-compress</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.ivy</groupId>
					<artifactId>ivy</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.zookeeper</groupId>
					<artifactId>zookeeper</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.curator</groupId>
					<artifactId>apache-curator</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.curator</groupId>
					<artifactId>curator-framework</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.codehaus.groovy</groupId>
					<artifactId>groovy-all</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.calcite</groupId>
					<artifactId>calcite-core</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.calcite</groupId>
					<artifactId>calcite-druid</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.calcite.avatica</groupId>
					<artifactId>avatica</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.apache.calcite</groupId>
					<artifactId>calcite-avatica</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.google.code.gson</groupId>
					<artifactId>gson</artifactId>
				</exclusion>
				<exclusion>
					<groupId>stax</groupId>
					<artifactId>stax-api</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.google.guava</groupId>
					<artifactId>guava</artifactId>
				</exclusion>
			</exclusions>
		</dependency>


		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-runtime-web_2.11</artifactId>
			<version>${flink.version}</version>
		</dependency>

		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-connector-kafka_${scala.binary.version}</artifactId>
			<version>${flink.version}</version>
		</dependency>


		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-json</artifactId>
			<version>${flink.version}</version>
		</dependency>

		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-jdbc_2.11</artifactId>
			<version>${flink.version}</version>
		</dependency>

		<!-- Apache Flink dependencies -->
		<!-- These dependencies are provided, because they should not be packaged into the JAR file. -->

		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-table-planner-blink_2.11</artifactId>
			<version>${flink.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-streaming-java_${scala.binary.version}</artifactId>
			<version>${flink.version}</version>
			<scope>provided</scope>
		</dependency>