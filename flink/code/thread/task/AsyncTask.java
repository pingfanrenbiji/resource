package com.cunguan.seceum.config.thread.task;

import com.cunguan.seceum.mq.rabbitmq.message.MessageFlink;
import com.cunguan.seceum.service.FlinkCsvService;
import com.cunguan.seceum.util.json.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AsyncTask {

    /**
     * myAsyncTaskPool 线程池的方法名，此处如果不写，会使用Spring默认的线程池
     * @param i
     */
    @Async("myAsyncTaskPool")
    public void run(int i){
       log.info("我是：" + i);
    }

    @Async
    public void run1(int i){
        log.info("我是：" + i);
    }

    @Autowired
    FlinkCsvService flinkCsvService;

    /**flink处理csv**/
    @Async
    public void flinkDeal(MessageFlink messageFlink) throws Exception {
        log.info("==== AsyncTask flinkDeal 开启一个异步线程去处理,messageFlink:{}", JsonUtils.toJson(messageFlink));
        flinkCsvService.flinkDeal(messageFlink);
    }



}
