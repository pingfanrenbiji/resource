package com.cunguan.seceum.config.thread;

import com.cunguan.seceum.config.thread.task.AsyncTask;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @package: com.cunguan.seceum
 * @description:
 * @author: 孟凡霄
 * @date: Created in 2021/1/19 6:37 下午
 * @copyright: Copyright (c) 2020
 * @version: V1.0
 * @modified: 孟凡霄
 */


public class ThreadTest {

    @Autowired
    private AsyncTask asyncTask;

    void test(){
        for (int i = 0; i < 100; i++) {
            asyncTask.run(i);
        }
    }

    void test1(){
        for (int i = 0; i < 100; i++) {
            asyncTask.run1(i);
        }
    }

}
