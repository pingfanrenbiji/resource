package com.cunguan.seceum.mq.rabbitmq.handler;

import cn.hutool.json.JSONUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.cunguan.seceum.config.thread.task.AsyncTask;
import com.cunguan.seceum.flink.json.JsonUtils;
import com.cunguan.seceum.mq.rabbitmq.constants.MqConstants;
import com.cunguan.seceum.mq.rabbitmq.message.MessageFlink;
import com.cunguan.seceum.service.FlinkCsvService;
import com.cunguan.seceum.util.file.AlluxioUtils;
import com.rabbitmq.client.Channel;
import com.cunguan.seceum.mq.rabbitmq.constants.RabbitConsts;
import com.cunguan.seceum.mq.rabbitmq.message.MessageStruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * <p>
 * 直接队列1 处理器
 * </p>
 *
 * @package: com.cunguan.seceum.mq.rabbitmq.handler
 * @description: 直接队列1 处理器
 * @author: 孟凡霄
 * @date: Created in 2019-01-04 15:42
 * @copyright: Copyright (c) 2019
 * @version: V1.0
 * @modified: 孟凡霄
 */
@Slf4j
//@RabbitListener(queues = MqConstants.QUEUE_TEST)
@RabbitListener(queues = "${java.flink.queue}")
@Component
public class FlinkDirectQueueOneHandler {

    @Autowired
    FlinkCsvService flinkCsvService;

    @Autowired
    private AsyncTask asyncTask;

    /**
     * 如果 spring.rabbitmq.listener.direct.acknowledge-mode: auto，则可以用这个方式，会自动ack
     */
     @RabbitHandler
    public void directHandlerAutoAck(Object message, Message msg,Channel channel) throws Exception {

         //  如果手动ACK,消息会被监听消费,但是消息在队列中依旧存在,如果 未配置 acknowledge-mode 默认是会在消费完毕后自动ACK掉
         final long deliveryTag = msg.getMessageProperties().getDeliveryTag();
         try {

             String messageString=JsonUtils.toJson(message);
             Message message1=JsonUtils.fromJsonObject(messageString,Message.class);
             String message2 = new String(message1.getBody(), "UTF-8");
             log.info("直接队列处理器，接收消息：{}", message2);

             MessageFlink messageFlink=JsonUtils.fromJsonObject(message2,MessageFlink.class);

             /**从线程池中获取一个线程**/
             asyncTask.flinkDeal(messageFlink);

             // 通知 MQ 消息已被成功消费,可以ACK了
             channel.basicAck(deliveryTag, false);
         } catch (Exception e) {

             try {
                 // 处理失败,重新压入MQ
                 channel.basicRecover();
             } catch (IOException e1) {
                 e1.printStackTrace();
             }
         }

    }


//    @RabbitHandler
    public void directHandlerManualAck(MessageStruct messageStruct, Message message, Channel channel) {
        //  如果手动ACK,消息会被监听消费,但是消息在队列中依旧存在,如果 未配置 acknowledge-mode 默认是会在消费完毕后自动ACK掉
        final long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            log.info("直接队列1，手动ACK，接收消息：{}", JSONUtil.toJsonStr(messageStruct));
            // 通知 MQ 消息已被成功消费,可以ACK了
            channel.basicAck(deliveryTag, false);
        } catch (IOException e) {
            try {
                // 处理失败,重新压入MQ
                channel.basicRecover();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
