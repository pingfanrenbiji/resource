package com.cunguan.seceum.mq.rabbitmq.handler;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @package: com.cunguan.seceum
 * @description:
 * @author: 孟凡霄
 * @date: Created in 2021/1/19 3:47 下午
 * @copyright: Copyright (c) 2020
 * @version: V1.0
 * @modified: 孟凡霄
 */


public class ConnectionUtil {

    public static Connection getConnection() throws IOException, TimeoutException {
        //定义连接工厂
        ConnectionFactory factory = new ConnectionFactory();

        //设置服务地址
        factory.setHost("127.0.0.1");
//        factory.setHost("101.132.179.55");

        //端口
        factory.setPort(5672);

        //设置账号：用户名、密码（不设置为默认账户密码）
        //factory.setUsername("guest");
        //factory.setPassword("guest");

        //通过工厂获取连接
        Connection connection = factory.newConnection();
        return connection;
    }

}
