package com.cunguan.seceum.mq.rabbitmq.handler;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @package: com.cunguan.seceum
 * @description:
 * @author: 孟凡霄
 * @date: Created in 2021/1/19 3:52 下午
 * @copyright: Copyright (c) 2020
 * @version: V1.0
 * @modified: 孟凡霄
 */


public class Send {

    private final static String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = ConnectionUtil.getConnection();

        try {

            Channel channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = "Hello World!";
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));

            System.out.println(" [x] Sent '" + message + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            connection.close();
        }

    }
}
